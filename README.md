# Where you Belong - Front end

## Installation
Install a webserver (Nginx or Apache)
```
$ sudo apt-get install nginx 
```

Add a site to the web server serving the front_end directory
In nginx changing the root in conf file is sufficient 

```
root=/usr/share/nginx/front_end/;
```

If the backend is installed on another machine, modify the addressBackend variable in where_you_belong.js to point to the backend

That's all ! Enjoy
